const Discord = require("discord.js")
var client = new Discord.Client()
var re = Discord.RichEmbed

var db = require("quick.db")

const cfg = require("./config.json")
client.cfg = cfg

client.on("message", message => {
    var args = message.content.split(" ").slice(1)
    var cmd = message.content.split(" ")[0].substr(cfg.prefix.length)

    var executeCommands = () => {
        if (cmd === "help") {
            let helpEmbed = new re()
                .setTitle("Helpliste")
                .addField("help", "Wer weiß...")
                .addField("purge-mentions", "Löscht die unerlaubten Erwähnungen für einen Nutzer")
                .setAuthor("Version " + cfg.version)
            message.channel.send(helpEmbed)
        } else if (cmd === "purge-mentions") {
            if (args[0]) {
                var target = message.mentions.users.first() || client.users.get(args[0])
                let purgeEmbed = new re()
                    .setTitle("Unerlaubte Erwähnungen löschen")
                    .setDescription("Erfolgreich unerlaubte Erwähnungen für " + target.username + " gelöscht!")
                    .setColor(0x7300FF)
                db.set("mentions_" + message.guild.id + "_" + target.id, 0)
                message.channel.send(purgeEmbed)
                return;
            }
            let purgeErrorEmbed = new re()
                .setColor(0xe60000)
                .setTitle("Falsche Formulierung")
                .setDescription("Format: `purge-mentions @nutzer` oder `purge-mentions @nutzerid`")
            message.channel.send(purgeErrorEmbed)
        }
    }

    cfg.protected.forEach(id => {
        if (message.content.includes(`<@${id}>`)) {
            let mentions = parseInt(db.get("mentions_" + message.guild.id + "_" + message.author.id)) || 0
            let embedMentioned = new re()
            embedMentioned
                .setColor(0x00e600)
                .setTitle("Neue Erwähnung")
                .addField("Nachricht", message.content)
                .addField("Nutzer", `<@${message.author.id}> (\`${message.author.id}\`)`)
                .setFooter(new Date())
            if (mentions !== 0) embedMentioned.addField("Dieser Nutzer hat davor so oft unerlaubt erwähnt:", mentions)
            message.delete()
            // Database Key f. mentions ist dann z.B. mentions_475747682460237840_299556333097844736 / mentions_serverID_nutzerID
            db.set("mentions_" + message.guild.id + "_" + message.author.id, (parseInt(mentions) + 1))
            client.channels.get(cfg.log.mentions).send(embedMentioned)
        }
    })

    if (message.content.startsWith(cfg.prefix)) executeCommands()
})

client.on("ready", () => {
    console.log("Bereit als " + client.user.tag + "!")
})

client.on("guildMemberAdd", member => {
    cfg.uniqueNames.forEach(name => {
        if (cfg.protected.includes(member.user.id)) return;
        if (member.user.username === name || member.nickname === name) {
            member.user.send("Hey. Du wurdest von **" + member.guild.name + "** gekickt, da dein Nutzername oder Nickname **" + name + "** war!")
            if (member.kickable) member.kick("Hieß " + name)
        }
    })
})

client.on("guildMemberUpdate", (o, n) => {
    cfg.uniqueNames.forEach(name => {
        if (cfg.protected.includes(n.user.id)) return;
        if (n.user.username.toLowerCase() === name || n.nickname.toLowerCase() === name) {
            n.user.send("Hey. Du wurdest von **" + n.guild.name + "** gekickt, da dein Nutzername oder Nickname **" + name + "** war!")
            if (member.kickable) member.kick("Hieß " + name)
        }
    })

})

client.login(cfg.token)